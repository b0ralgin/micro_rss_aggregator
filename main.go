package main

import (
	"context"
	"log"
	"os"
	"time"

	article "bitbucket.org/b0ralgin/micro_API/article"
	task "bitbucket.org/b0ralgin/micro_API/task"
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/client"
	"github.com/micro/go-plugins/broker/nats"
	"github.com/mmcdole/gofeed"
	natsio "github.com/nats-io/nats"
)

func FetchData(artChan chan article.Article) func(ctx context.Context, req *task.Task) error {
	return func(ctx context.Context, req *task.Task) error {
		log.Println("Start parsing")
		fp := gofeed.NewParser()
		feed, err := fp.ParseURL(req.GetUrl())
		if err != nil {
			return err
		}
		for _, feed := range feed.Items {
			res := article.Article{
				Title:   feed.Title,
				Source:  req.GetUrl(),
				Content: feed.Description,
			}

			if feed.PublishedParsed != nil {
				res.PublishedAt = feed.PublishedParsed.Unix()
			} else {
				res.PublishedAt = time.Now().Unix()
			}
			artChan <- res
		}
		return nil
	}
}

func sendToDB(client client.Client, dbQueue string, queue chan article.Article) {
	p := micro.NewPublisher(dbQueue, client)
	for article := range queue {
		p.Publish(context.Background(), &article)
	}
}

func main() {
	natsQueue := os.Getenv("NATS_QUEUE")
	dbQueue := os.Getenv("DB_QUEUE")
	natsServer := os.Getenv("NATS_SERVER")
	article := make(chan article.Article)
	if natsQueue == "" || natsServer == "" {
		log.Fatal("wrong Envs")
	}
	broker := nats.NewBroker(nats.Options(natsio.Options{
		Url: natsServer,
	}))
	srv := micro.NewService(
		micro.Name("greeter"),
		micro.Broker(broker),
	)

	srv.Init()
	err := micro.RegisterSubscriber(natsQueue, srv.Server(), FetchData(article))
	if err != nil {
		log.Fatal(err)
	}
	go sendToDB(srv.Client(), dbQueue, article)
	if err := srv.Run(); err != nil {
		log.Fatal(err)
	}
}
